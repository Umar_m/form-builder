<?php

use App\FormElementsModel;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware'=>'api'],function (){

    Route::get('form-builder/', function () {
        return formElementsModel::latest()->orderBy('created_at', 'desc')->get();
     });


    // create single from Element
    Route::post('/form-builder/store', function (Request $request) {
        return FormElementsModel::create([
            'form_name'=>$request->input('form_name'),
            'form_elements'=>$request->input('form_elements')
        ]);
    });

     // delete single from Element
     Route::delete('/form-builder/{id}', function ($id) {
        return FormElementsModel::destroy($id);
    });


     // delete single from Element
     Route::patch('/form-builder/{id}', function (Request $request, $id) {
        return  response()->json(
            FormElementsModel::findOrFail($id)->update(
            ['form_name'=>$request->input('form_name'),
            'form_elements'=>$request->input('form_elements'),
        ]));
    });


});


