<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/formbuilder', 'FormBuilderController@index')->name('home');
Route::get('/formbuilder/add-form', 'FormBuilderController@addForm')->name('addform');
Route::get('/formbuilder/edit-form/{id}', 'FormBuilderController@editForm')->name('editform');
