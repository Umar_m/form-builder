<?php

namespace App\Http\Controllers;

use App\FormElementsModel;
use Illuminate\Http\Request;


class FormBuilderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $formElementesObj = new FormElementsModel;
         $formElementes = $formElementesObj::orderBy('id','ASC')->get();
        return view('formbuilder')->with('formElementes',$formElementes);
     }

    public function addForm()
    {
        return view('addform');
    }

    public function editForm($id)
    {
        $formElementesObj = new FormElementsModel;
        $formElementes = $formElementesObj::findorFail($id);

        return view('editform')->with('formElementes',$formElementes);
    }

}
