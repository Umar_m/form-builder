<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormElementsModel extends Model
{
    protected $fillable = ['form_name', 'form_elements'];

}
