
/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */
require('./bootstrap');
require('./jquery.min.js');
require('./jquery-ui.min.js');
require('./form-builder.min.js');
require('alertifyjs');
require('./custom.js');



