
jQuery(document).ready(function(){
    const alertify = require('alertifyjs');
    alertify.set('notifier','position', 'top-right');
    const fbEditor = document.getElementById("build-wrap");

    const options = {
        disabledActionButtons: ['data'],
        defaultFields: [{
            className: "form-control",
            label: "Form Name",
            placeholder: "Enter your Form name",
            name: "form-name",
            required: true,
            type: "text",
            disabledFieldButtons: ['remove','edit','copy'],
        }],
        disabledAttrs: ["access"], //removing role base edit
    };
    const formBuilders = $(fbEditor).formBuilder(options);

///////////////////////// FORM BUILDER AJAX FUNCTIONS GOES HERE

$('#build-wrap').on('click', '.save-template', function(e) {
    e.preventDefault();
    var formElements =formBuilders.actions.getData('json');
    axios.post('/api/form-builder/store',{
        form_name: jQuery("input[name='form-name-preview']" ).val(),
        form_elements: formElements,
      }).then(function(response){
        jQuery("input[name='form-name-preview']" ).val('');
        alertify.success('Form data Saved');
    }).catch(function (error) {
        console.log(error);
    })

});

//Remove

$('.forms-table').on('click', '.remove-form', function(e) {
    e.preventDefault();
    var id = jQuery(this).data('id');
    alertify.confirm('Delete Form','Do you want to delete form',
        function(){
            axios.delete('/api/form-builder/'+id)
            .then(function(response){
                $("#data-"+id).remove();
                alertify.success('Form data Saved');
            }).catch(function (error) {
                    console.log(error);
            })
         }, // Ok
        function(){} // Cancel
    );

});


//Edit
formData= jQuery('#edit-wrap').data('elements');
var id = jQuery('#edit-wrap').data('id');

var editOptions = {
    formData: formData,
    disabledActionButtons: ['data'],
};
var fbTemplate1 = jQuery(document.getElementById("edit-wrap"));
const formBuilder = jQuery(fbTemplate1).formBuilder(editOptions);

try {

} catch (err) {
    console.warn("formData not available yet.");
    console.error("Error: ", err);
}


formBuilder.promise.then(function(fb) {

    $('#edit-wrap').on('click', '.save-template', function(e) {
        e.preventDefault();
        var formElements = fb.formData;
        axios.patch('/api/form-builder/'+id,{
            form_name: jQuery("input[name='form-name-preview']" ).val(),
            form_elements: formElements,
        }).then(function(response){
            // jQuery("input[name='form-name-preview']" ).val('');
            alertify.success('Form data Saved');
        }).catch(function (error) {
            console.log(error);
        })

    });

});

});
