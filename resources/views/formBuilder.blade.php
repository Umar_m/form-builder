@extends('layouts.master')

@section('content')


    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            All Form
            <small>All forms here</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-11">
                        <div class="row justify-content-center">
                                <div class="col-md-11 table-bg">
                                         <table class="table forms-table">
                                            <thead>
                                                <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Form Name</th>
                                                    <th scope="col">Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($formElementes as $element)
                                                <tr id="data-{{ $element->id }}">
                                                <th scope="row">{{ $element->id }}</th>
                                                <td> {{ ucfirst($element->form_name) }}</td>
                                                    <td>
                                                        <span class="badge fa badge-pill">
                                                        <a href="/formbuilder/edit-form/{{ $element->id }}" >
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <a href="#" data-id="{{ $element->id }}" class="mx-2 remove-form">
                                                            <i class="fa fa-trash "></i>
                                                        </a>
                                                    </span>
                                                </td>
                                                </tr>
                                                @endforeach

                                            </tbody>
                                        </table>
                                </div>
                        </div>
                </div>
            </div>
        </div>
    </section>

@endsection
