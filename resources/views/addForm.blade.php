@extends('layouts.master')

@section('content')

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    Add Form
                    <small>You can create muliple forms from here</small>
                </h1>
            </section>

            <!-- Main content -->
            <section class="content container-fluid">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-11">
                                <div id="build-wrap"></div>
                        </div>
                    </div>
                </div>
            </section>

@endsection
